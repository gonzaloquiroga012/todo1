'use strict'
var mockupUser = require('./mockupUser');

var User = function(userData){
    this.id = userData.id;
    this.name = userData.name;
    this.surname = userData.surname;
    this.email = userData.email;
    this.username = userData.username;
    this.password = userData.password;
};

User.find = function(data, cb){
    /** just a mockup */
    if(data.username == mockupUser.username){
        let res = new User({
            id: mockupUser.id, 
            name: mockupUser.name,
            surname: mockupUser.surname,
            email: mockupUser.email,
            username: mockupUser.username,
            password: mockupUser.password
        })
        cb(res,null); // function(result,err)
    }else{
        cb(null,null);
    }
}

module.exports = User;
