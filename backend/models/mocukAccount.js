module.exports = MockupAccount1 = {
    id: "101",
    alias: "terminal",
    typeAccount: {
        name: "Cuenta de ahorro",
        code: "CA",
        currency: "COP"
    },
    owner: "1",
    number: "1*****3056",
    balance: "11340000",
}

module.exports = MockupAccount2 = {
    id: "105",
    alias: "power",
    typeAccount: {
        name: "Cuenta Corriente",
        code: "CC",
        currency: "COP"
    },
    owner: "1",
    number: "2*****3070",
    balance: "22340000",
}

module.exports = MockupAccount3 = {
    id: "140",
    alias: "roca",
    typeAccount: {
        name: "Cuenta de ahorros",
        code: "CC",
        currency: "COP"
    },
    owner: "1",
    number: "1*****3056",
    balance: "11340000",
}


module.exports = MockupAccountListUser1 = [
    MockupAccount1,
    MockupAccount2,
    MockupAccount3
]