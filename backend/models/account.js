'use strict'
const mockupUser = require('./mockupUser');
const MockupAccountListUser1 = require('./mocukAccount')

var Account = function (AccountData) {
    this.id = AccountData.id;
    this.alias = AccountData.alias;
    this.typeAccount = AccountData.typeAccount;
    this.owner = AccountData.owner;
    this.number = AccountData.number;
    this.balance = AccountData.balance;
};

Account.findByUserId = function (data, cb) {
    /** just a mockup */
    if (data.id == mockupUser.id) {
        let res = [];
        MockupAccountListUser1.forEach(element => {
            res.push(new Account(element));
        });
        cb(res, null);
    } else {
        cb(null, null);
    }
}

Account.findById = function (data, cb) {
    /** just a mockup */
    let res = null;
    for (const element of MockupAccountListUser1) {
        if (element.id == data.id) {
            res = new Account(element);
            break;
        }
    }
    cb(res, null);
}

module.exports = Account;
