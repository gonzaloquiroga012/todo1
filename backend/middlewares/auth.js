'use strict'

const AuthService = require('../services/authService')

function Auth(){}

Auth.isAuth = function(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'No tienes autorización' })
  }

  const token = req.headers.authorization.split(' ')[1]

  AuthService.decodeToken(token)
    .then(response => {
      req.user = response
      next()
    })
    .catch(response => {
      res.status(response.status)
      res.send({message: response.message});
    })
}

module.exports = Auth;
