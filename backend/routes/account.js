var express = require('express');
var router = express.Router();
const Auth = require('../middlewares/auth');
const AccountController = require('../controllers/AccountController');

router.get('/:id', Auth.isAuth, AccountController.findById);

// router.post('/',/*Auth.isAuth,*/ function(req, res, next) {
//   User.find({username: req.body.username},(result,err)=>{
//       res.send(result);
//   })
// });

module.exports = router;
