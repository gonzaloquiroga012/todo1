var express = require('express');
var router = express.Router();
const AccountController = require('../controllers/AccountController');
const Auth = require('../middlewares/auth');

/* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

// router.post('/',Auth.isAuth, function(req, res, next) {
//   User.find({username: req.body.username},(result,err)=>{
//       res.send(result);
//   })
// });

router.get('/:id/accounts', Auth.isAuth, AccountController.findByUserId);

module.exports = router;
