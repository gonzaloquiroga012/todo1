var express = require('express');
var router = express.Router();
const AuthController = require('../controllers/AuthController')
const Auth = require('../middlewares/auth');

/* GET home page. */
router.get('/',Auth.isAuth,function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/signIn',AuthController.signIn);

module.exports = router;
