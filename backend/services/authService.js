'use strict'

const jwt = require('jwt-simple')
const moment = require('moment')
const config = require('../config')
const User = require('../models/user');
const AuthService = function () { }

AuthService.createToken = function (user) {
  const payload = {
    sub: user.id,
    iat: moment().unix(),
    exp: moment().add(5, 'minutes').unix()
  }

  return jwt.encode(payload, config.SECRET_TOKEN)
}

AuthService.decodeToken = function (token) {
  const decoded = new Promise((resolve, reject) => {
    try {
      const payload = jwt.decode(token, config.SECRET_TOKEN,true)
      if (payload.exp <= moment().unix()) {
        reject({
          status: 401,
          message: 'El token ha expirado'
        })
      }
      resolve(payload.sub)
    } catch (err) {
      reject({
        status: 500,
        message: 'Invalid Token'
      })
    }
  })

  return decoded
}

AuthService.authenticate = function (data) {
  const authenticate = new Promise((resolve, reject) => {
    User.find({ username: data.username }, (user, err) => {
      if (err) {
        reject({ message: err });
      } else {
        if (user) {
          if (user.password != data.password) {
            reject({ message: 'Contraseña incorrecta' });
          } {
            let token = AuthService.createToken(user);
            delete user.password;
            resolve({ token, user });
          }
        } else {
          reject({ message: 'Usuario no encontrado' });
        }
      }
    })
  });

  return authenticate;
}

module.exports = AuthService;
