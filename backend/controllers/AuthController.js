'use strict'
const  User = require('../models/user');
const AuthService = require('../services/authService');

const AuthController = function(){}

AuthController.signIn = function(req, res) {
   AuthService.authenticate(req.body).then(result=>{
       res.send(result);
   }).catch(err=>{
       res.send(err);
   })
}
module.exports = AuthController;
