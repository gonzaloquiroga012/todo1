'use strict'
const  Account = require('../models/account');

const AccountController = function(){}

AccountController.findByUserId = function(req, res) {
    Account.findByUserId({id:req.params.id},(result,err)=>{
        res.send(result);
    });
}

AccountController.findById = function(req, res) {
    Account.findById({id:req.params.id},(result,err)=>{
        res.send(result);
    });
}

module.exports = AccountController;
