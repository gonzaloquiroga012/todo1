import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QRService } from '../services/qr.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-options-qr',
  templateUrl: './options-qr.page.html',
  styleUrls: ['./options-qr.page.scss'],
})
export class OptionsQRPage implements OnInit {
  loading: boolean = false;
  dataReaded: any;
  transferForm: FormGroup;
  qrCode: string;
  reading: boolean = false;
  showQr: boolean = false;
  accounts: Array<Account> = [];

  currencyList = [{ name: 'Peso Colombiano', code: 'COP' }, { name: 'Dolar', code: 'USD' }];

  constructor(
    public formBuilder: FormBuilder,
    public qrSrv: QRService,
    public alertController: AlertController,
    private accountSrv: AccountService,
    public loadingController: LoadingController
  ) {
    this.loading = true;
    this.presentLoading().then(() => {
      this.loadAccounts();
    })
    this.initForm();
    switch (this.qrSrv.currentAction) {
      case 'read':
        this.readQR();
        break;
      case 'generate':
        this.reading = false;
        break;
      default:
        break;
    }
  }

  ngOnInit() { }

  ionViewWillLeave() {
    this.closeLoading();
  }

  private closeLoading() {
    if (this.loading == true) {
      this.loadingController.dismiss().then(() => {
        this.loading = false;
      })
    }
  }

  private loadAccounts() {
    this.accountSrv.getAccounts()
      .then((result: Array<Account>) => {
        this.accounts = result;
      })
      .catch((err) => {
        this.presentAlert("Se ha producido un error.");
      })
      .finally(() => {
        this.closeLoading();
      });
  }

  private initForm() {
    this.transferForm = this.formBuilder.group({
      idAccountFrom: ['', Validators.compose([Validators.required])],
      numberAccountTo: ['', Validators.compose([Validators.required])],
      currency: ['', Validators.compose([Validators.required])],
      especifiedAmount: [false, Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.pattern(/^[1-9]\d*(\.\d+)?$/), Validators.required, Validators.min(1)])]
    });
  }

  public readQR() {
    this.reading = true;
    this.qrSrv.getCode()
      .then((res: string) => {
        this.setData(res);
      })
      .catch((error) => {
        console.log(error)
      })
      .finally(() => {
        this.stop();
      });
  }

  public generateQR() {
    //if (this.transferForm.valid) {
    this.showQr = true;
    console.log(this.getString());
    this.qrCode = this.getString();
    //}
  }

  public stop() {
    this.qrSrv.stopRead();
    this.reading = false;
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  private async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'cargando',
      duration: 5000,
    });
    await loading.present();
  }

  public hideQR() {
    this.showQr = false;
  }

  private getString() {
    let _return = '';
    _return = _return + this.transferForm.value.idAccountFrom.id + " ";
    _return = _return + this.transferForm.value.idAccountFrom.number + " ";
    _return = _return + this.transferForm.value.especifiedAmount + " ";
    _return = _return + this.transferForm.value.currency + " ";
    _return = _return + this.transferForm.value.amount;
    console.log(_return);
    return _return;
  }

  private setData(stringData: string) {
    try {
      let data = stringData.split(" ");
      let numberAccountTo = this.transferForm.controls.numberAccountTo;
      let especifiedAmount = this.transferForm.controls.especifiedAmount;
      let currency = this.transferForm.controls.currency;
      let amount = this.transferForm.controls.amount;

      numberAccountTo.setValue(data[1]);
      let esp = (data[2] == 'true' ? true : false);
      especifiedAmount.setValue(esp);

      if (especifiedAmount.value == true) {
        currency.setValue(data[3]);
        amount.setValue(data[4]);
      }
      if (numberAccountTo.invalid || (especifiedAmount.value == true && (currency.invalid || amount.invalid))) {
        this.transferForm.reset();
        throw "error";
      }
    } catch (error) {
      this.presentAlert('El código no es válido!')
    }
  }

  public validGenerate() {
    if (this.transferForm.controls.idAccountFrom.valid && (this.transferForm.controls.especifiedAmount.value == false || (this.transferForm.controls.currency.valid && this.transferForm.controls.amount.valid))) {
      return true;
    } else {
      return false;
    }
  }

  public transference() {
    this.presentAlert('Transferencia realizada con éxito');
  }
}
