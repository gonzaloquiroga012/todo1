import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { OptionsQRPageRoutingModule } from './options-qr-routing.module';
import { OptionsQRPage } from './options-qr.page';
import { HeaderModule } from '../components/header/header.module';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    ReactiveFormsModule,
    OptionsQRPageRoutingModule,
    QRCodeModule
  ],
  declarations: [OptionsQRPage]
})
export class OptionsQRPageModule { }
