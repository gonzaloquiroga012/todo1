const baseUrl = 'https://cors-anywhere.herokuapp.com/https://tecnologias-mobiles-service.herokuapp.com/';//'http://localhost:3000/';
const weatherServiceUrl = 'https://cors-anywhere.herokuapp.com/https://api.openweathermap.org/data/2.5/weather';
const iconWeatherUrl = 'http://openweathermap.org/img/wn/icon@2x.png'
const WeatherAppid = '72ffed50de2489e1dd9daff290aaff5e';
const lang = 'sp';
const units = 'metric';

export { baseUrl, weatherServiceUrl, WeatherAppid, iconWeatherUrl, lang, units }