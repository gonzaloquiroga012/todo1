import { Component, OnInit, Input } from '@angular/core';
import { iconWeatherUrl } from "../../app.config";

@Component({
  selector: 'app-weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss'],
})
export class WeatherWidgetComponent implements OnInit {
  
  @Input() currentWeather: any;

  constructor() { }

  ngOnInit() {}

  getIcon(iconName){
    let urlIcon = iconWeatherUrl;
    urlIcon = urlIcon.replace('icon',iconName);
    return urlIcon;
  }

}
