import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() title: string; 
  alertController: any;
  constructor(
    public AuthSrv: AuthService,
    public AlertCrtl: AlertController
    ) {}

  ngOnInit() {}

  logout(){
    this.presentAlertConfirm();
  }

  async presentAlertConfirm() {
    const alert = await this.AlertCrtl.create({
      header: 'Confirmar',
      message: '¿Esta seguro que desea cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {}
        }, {
          text: 'Confirmar',
          handler: () => {
            this.AuthSrv.logout();
          }
        }
      ]
    });

    await alert.present();
  }

}
