export interface TypeAccount{
    name: string;
    code: string;
    currency: string;
}