import { TypeAccount } from "./typeAccount";

export interface Account {
    id: number;
    alias: String;
    type: TypeAccount;
    owner: number;
    number: number;
    balance: number;
}