import { Injectable } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

@Injectable({
  providedIn: 'root'
})
export class QRService {

  currentAction: string;

  constructor(private qrScanner: QRScanner) { }

  getCode() {
    return new Promise((resolve, reject) => {
      this.qrScanner.prepare()
        .then((status: QRScannerStatus) => {
          if (status.authorized) {
            this.qrScanner.show();
            let scanSub = this.qrScanner.scan().subscribe((text: string) => {
              this.qrScanner.hide();
              scanSub.unsubscribe();
              resolve(text);
            });
          } else if (status.denied) {
            reject("Permisos denegados");
          } else {
            reject("Permisos denegados");
          }
        })
        .catch((e: any) => reject(e));
    });
  }

  stopRead() {
    this.qrScanner.hide();
    this.qrScanner.destroy();
  }
}
