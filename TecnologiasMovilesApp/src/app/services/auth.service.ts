import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from '../app.config';
import { Session } from '../models/session';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public logged: boolean = false;

  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService,
    private router: Router
  ) { }

  public login(dataUser) {
    return new Promise((resolve, reject) => {
      this.http.post<Session>(baseUrl + 'signIn', dataUser)
        .subscribe((res) => {
          if (res.user) {
            this.localStorage.set("session", res);
            this.logged = true;
            resolve();
          } else {
            reject(res);
          }
        }, err => {
          reject(err);
        })
    });
  }

  public isLogged() {
    this.logged = this.localStorage.get('session') ? true : false;
    return this.logged;
  }

  public logout() {
    this.localStorage.remove('session');
    this.logged = false;
    this.router.navigateByUrl('/login');
  }
}
