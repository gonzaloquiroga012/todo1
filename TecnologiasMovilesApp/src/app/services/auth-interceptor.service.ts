import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'angular-2-local-storage';
import { Session } from '../models/session';
import { map, catchError } from 'rxjs/internal/operators';
import { AuthService } from './auth.service';
import { Events } from '@ionic/angular';
import { weatherServiceUrl } from '../app.config';
@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(
    private localStorage: LocalStorageService,
    private AuthSrv: AuthService,
    private events: Events
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const session: Session = this.localStorage.get('session');

    let request = req;

    if (session != null && request.url != weatherServiceUrl) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${session.token}`
        }
      });
    }

    return next.handle(request).pipe(catchError((err, caught) => {
      if (err.status === 401 && request.url != weatherServiceUrl) {
        this.events.publish('sessionExpirated');
        this.AuthSrv.logout();
      }

      return new Observable<HttpEvent<any>>();
    }))
  }

}
