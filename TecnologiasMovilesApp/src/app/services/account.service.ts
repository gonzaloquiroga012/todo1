import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { baseUrl } from '../app.config';
import { LocalStorageService } from 'angular-2-local-storage';
import { Session } from '../models/session';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private _session: Session = null;
  public get session(): Session {
    if (this._session == null) {
      this._session = this.localStorage.get("session");
    }
    return this._session;
  }

  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService
  ) { }

  public getAccounts() {
    return new Promise((resolve, reject) => {
      let id = this.session.user.id;
      this.http.get<Array<Account>>(baseUrl + `users/${id}/accounts`)
        .subscribe((res: Array<Account>) => {
          resolve(res);
        }, err => {
          reject(err);
        })
    });
  }

  public getUser() {
    return this.session.user;
  }
}
