import { TestBed } from '@angular/core/testing';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { QRService } from './qr.service';

describe('QRService', () => {
  beforeEach(() => TestBed.configureTestingModule({providers:[QRScanner]}));

  it('should be created', () => {
    const service: QRService = TestBed.get(QRService);
    expect(service).toBeTruthy();
  });
});
