import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { WeatherService } from './weather.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

describe('WatherService', () => {
  beforeEach(() => TestBed.configureTestingModule({ imports:[HttpClientModule],providers:[Geolocation]}));

  it('should be created', () => {
    const service: WeatherService = TestBed.get(WeatherService);
    expect(service).toBeTruthy();
  });
});
