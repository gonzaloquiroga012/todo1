import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { WeatherAppid, weatherServiceUrl, lang, units } from '../app.config'
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
    public http: HttpClient,
    private geolocation: Geolocation
  ) { }

  getCurrentWeather() {
    return new Promise((resolve, reject) => {

      this.geolocation.getCurrentPosition().then((resp) => {
        let params = new HttpParams()
          .set('lat', resp.coords.latitude.toString())
          .set('lon', resp.coords.longitude.toString())
          .set('lang', lang)
          .set('units', units)
          .set('appid', WeatherAppid);

        this.http.get(weatherServiceUrl, { params }).subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        })
      }).catch((error) => {
        reject(error);
      });
    });
  }


}
