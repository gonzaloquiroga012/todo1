import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
​
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LocalStorageModule } from 'angular-2-local-storage';
import { RouteReuseStrategy } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
​
import { LoginPage } from './login.page';
import { LoginPageRoutingModule } from './login-routing.module';
​
describe('Login Page', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
​
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [IonicModule.forRoot(),CommonModule,FormsModule,
        ReactiveFormsModule, AppRoutingModule, HttpClientModule,LocalStorageModule.forRoot({
          prefix: 'todo-app',
          storageType: 'localStorage'
        }),LoginPageRoutingModule
      ],
      providers: [
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
      ]
    }).compileComponents();
​
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));
​
  it('should create', () => {
    expect(component).toBeTruthy();
  });
​
  it('should username field have at last 6 characters', () => {
    component.loginForm.controls["username"].setValue("gonzatuc2020");
    expect(component.loginForm.controls["username"].value.length)
      .toBeGreaterThanOrEqual(6);
  })
​
  it('should username field have at max 12 characters', () => {
    component.loginForm.controls["username"].setValue("gonzatuc2020");
    expect(component.loginForm.controls["username"].value.length)
      .toBeLessThanOrEqual(12);
  })
​
  it('should password field have at last 6 characters', () => {
    component.loginForm.controls["password"].setValue("Invierno2020");
    expect(component.loginForm.controls["password"].value.length)
      .toBeGreaterThanOrEqual(6);
  })
​
});