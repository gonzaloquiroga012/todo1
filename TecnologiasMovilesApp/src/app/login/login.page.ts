import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    private AuthSrv: AuthService,
    public alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController
  ) {
    this.InitForm();
  }
  ngOnInit() { }

  InitForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(12)])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  login() {
    this.presentLoading().then(() => {
      let dataForm = this.loginForm.value;
      this.AuthSrv.login(dataForm).then(() => {
        this.router.navigate(['/']);
      }).catch(err => {
        let errorMessage = err.message ? err.message : 'Se ha producido un error';

        this.presentAlert(errorMessage);
      }).finally(() => {
        this.loadingController.dismiss();
      })
    });
  }

  private async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'cargando'
    });
    await loading.present();
  }

  async presentAlert(message) {
    const alert = await this.alertController.create({
      header: 'Error',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }
}
