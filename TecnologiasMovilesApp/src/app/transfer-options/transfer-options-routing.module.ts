import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TransferOptionsPage } from './transfer-options.page';

const routes: Routes = [
  {
    path: '',
    component: TransferOptionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TransferOptionsPageRoutingModule {}
