import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransferOptionsPageRoutingModule } from './transfer-options-routing.module';

import { TransferOptionsPage } from './transfer-options.page';
import { HeaderModule } from '../components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    TransferOptionsPageRoutingModule
  ],
  declarations: [TransferOptionsPage]
})
export class TransferOptionsPageModule {}
