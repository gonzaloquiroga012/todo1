import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QRService } from '../services/qr.service';

@Component({
  selector: 'app-transfer-options',
  templateUrl: './transfer-options.page.html',
  styleUrls: ['./transfer-options.page.scss'],
})
export class TransferOptionsPage implements OnInit {

  constructor(
    public route: Router,
    public QRsrv: QRService
  ) { }

  ngOnInit() {
  }
  public generateQR() {
    this.QRsrv.currentAction = 'generate';
    this.route.navigateByUrl('options-qr');
  }

  public readQR() {
    this.QRsrv.currentAction = 'read';
    this.route.navigateByUrl('options-qr');
  }

}
