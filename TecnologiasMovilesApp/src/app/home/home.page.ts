import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { Account } from "../models/account";
import { LoadingController, AlertController } from '@ionic/angular';
import { User } from '../models/user';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  user: User;
  accounts: Array<Account>;
  loading: boolean = false;

  constructor(
    public accountSrv: AccountService,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) { }

  ngOnInit(): void {
    this.Init();
  }

  ionViewDidEnter() {
    if (!this.loading) {
      this.Init();
    }
  }

  ionViewWillLeave() {
    this.closeLoading();
  }

  private Init() {
    this.loading = true;
    this.presentLoading().then(() => {
      this.loadAccounts();
      this.user = this.accountSrv.getUser();
    })
  }

  private loadAccounts() {
    this.accountSrv.getAccounts().then((result: Array<Account>) => {
      this.accounts = result;
    }).catch((err) => {
      this.presentAlert();
    })
      .finally(() => {
        this.closeLoading();
      });
  }

  private closeLoading() {
    if (this.loading == true) {
      this.loadingController.dismiss().then(() => {
        this.loading = false;
      })
    }
  }

  private async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'cargando',
      duration: 10000,
    });
    await loading.present();
  }

  private async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'Se ha producido un error al intentar cargar los datos.',
      buttons: ['OK']
    });
    await alert.present();
  }

  public getTitle() {
    return 'Bienvenido ' + this.user.name;
  }
}
