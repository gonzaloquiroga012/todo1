import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WeatherPageRoutingModule } from './weather-routing.module';

import { WeatherPage } from './weather.page';
import { WeatherWidgetComponent } from '../components/weather-widget/weather-widget.component';
import { HeaderModule } from '../components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    WeatherPageRoutingModule
  ],
  declarations: [WeatherPage, WeatherWidgetComponent]
})
export class WeatherPageModule { }
