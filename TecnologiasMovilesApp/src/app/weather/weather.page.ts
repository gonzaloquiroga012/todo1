import { Component, OnInit, AbstractType } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { LoadingController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.page.html',
  styleUrls: ['./weather.page.scss'],
})
export class WeatherPage implements OnInit {

  currentWeatherData: any;
  error: boolean = false;
  constructor(
    public weatherSrv: WeatherService,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) { }

  ngOnInit() {
    this.init();
  }
  init() {
    this.presentLoading().then(() => {
      this.weatherSrv.getCurrentWeather().then(data => {
        this.currentWeatherData = data;
      }).catch(err => {
        this.presentAlert();
        this.error = true;
      }).finally(() => {
        this.loadingController.dismiss();
      })
    });
  }
  private async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'cargando'
    });
    await loading.present();
  }

  private async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'Se ha producido un error al intentar cargar los datos.',
      buttons: ['OK']
    });

    await alert.present();
  }


}
