import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class loggedGuard implements CanActivate {

    constructor(private authSrv: AuthService, private router: Router) { }

    canActivate() {
        if (this.authSrv.isLogged()) {
            this.router.navigateByUrl('/');
            return false;
        }
        return true;
    }
}